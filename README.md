building instructions:

server:
disable both PlayerControllers and the Spawner.
activate the ServerCamera

leap client:
disable vivePlayerController and the serverCamera,
deactivate the vive checkbox in the Spawner Object

activate leapPlayerController

vive client:
disable leapPlayerController and the serverCamera, 
activate vivePlayerController
activate the vive checkbox in the Spawner Object
