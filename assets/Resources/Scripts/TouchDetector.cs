﻿using UnityEngine;
using System.Collections;

public class TouchDetector : MonoBehaviour
{
    public GameObject touched = null;

    void OnTriggerEnter(Collider c)
    {
        touched = c.gameObject;
    }

    void OnTriggerExit(Collider c)
    {
        touched = null;
    }
}
