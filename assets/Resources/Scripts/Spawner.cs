﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Leap.Unity;
using System;

public class Spawner : MonoBehaviour
{
    [SerializeField]
    private PinchDetector left;

    [SerializeField]
    private PinchDetector right;

    [SerializeField]
    private LocalPlayerController vivePlayerController;

    [SerializeField]
    private LocalPlayerController leapPlayerController;

    public bool vive = false;

    bool leftPinch = false;
    public Actor actor;
    AuthorityManager am;
    bool grabbed = false;
    bool nobox = true;

    void Update()
    {

        if (left.DidStartPinch || left.IsHolding)
        {
            leftPinch = true;
           // nobox = false;
        }


            if (left.DidRelease)
        {
            leftPinch = false;
            nobox = true;
        }
           

        if (leftPinch && nobox)
        {
            nobox = false;

            actor.GetComponent<Actor>().CmdSpawn(left.transform.position);      
        }
    }

    public void AssignProperties(GameObject go)
    {
        go.GetComponent<AuthorityManager>().AssignActor(actor);
        AuthorityManager am = go.GetComponent<AuthorityManager>();
        am.grabbedByPlayer = true;

        if (vive)
        {

         //   spawner = GameObject.Find("VivePlayerController").GetComponent<Spawner>();
            go.GetComponent<ViveGrab>().localPlayerController = vivePlayerController;
            go.GetComponent<ViveGrab>().enabled = true;
        }
        else
        {
            go.GetComponent<LeapGrab>().localPlayerController = leapPlayerController;
            go.GetComponent<LeapGrab>().right = right;
            go.GetComponent<LeapGrab>().enabled = true;
        }

        am.releasedByPlayer = true;
    }

    public static explicit operator Spawner(GameObject v)
    {
        throw new NotImplementedException();
    }
}
