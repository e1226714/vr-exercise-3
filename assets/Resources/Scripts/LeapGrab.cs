﻿using UnityEngine;
using System.Collections;
using Leap.Unity;

// This script defines conditions that are necessary for the Leap player to grab a shared object
// TODO: values of these four boolean variables can be changed either directly here or through other components
// AuthorityManager of a shared object should be notifyed from this script

public class LeapGrab : MonoBehaviour
{

    [SerializeField]
    public PinchDetector right;

    [SerializeField]
    public LocalPlayerController localPlayerController;

    AuthorityManager am;
    

    // conditions for the object control here
    bool rightHandTouching = false;
    bool rightPinch = false;
    bool grabbed = false;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //if (gameObject.Equals(localPlayerController.actor.GetComponent<Actor>().character.left.gameObject.GetComponent<TouchDetector>().touched))
        //    leftHandTouching = true;

        //else
        //    leftHandTouching = false;

        if (gameObject.Equals(localPlayerController.actor.GetComponent<Actor>().character.right.gameObject.GetComponent<TouchDetector>().touched))
            rightHandTouching = true;

        else
            rightHandTouching = false;

        if (right.DidStartPinch || right.IsHolding)
            rightPinch = true;

        if (right.DidRelease)
            rightPinch = false;


        if (rightHandTouching && rightPinch)
        {
            // notify AuthorityManager that grab conditions are fulfilled
            //am.grabbedByPlayer = true;
            //grabbed = true;
            if (!grabbed)
            {
                if (am == null)
                    am = gameObject.GetComponent<AuthorityManager>();

                am.grabbedByPlayer = true;
                grabbed = true;
                //gameObject.GetComponent<OnGrabbedBehaviour>().OnGrabbed();
                OnGrabbedBehaviour ogb = gameObject.GetComponent<OnGrabbedBehaviour>() as OnGrabbedBehaviour;
                ogb.localPlayerController = this.localPlayerController;
                ogb.OnGrabbed();
            }
        }


        else
        {
            // grab conditions are not fulfilled
            if (grabbed)
            {
                //am.releasedByPlayer = true;
                //grabbed = false;              
                if (am == null)
                    am = gameObject.GetComponent<AuthorityManager>();
                am.releasedByPlayer = true;
                grabbed = false;
                gameObject.GetComponent<OnGrabbedBehaviour>().OnReleased();
            }
        }
    }
}
