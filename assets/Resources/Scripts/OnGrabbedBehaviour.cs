﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

// TODO: define the behaviour of a shared object when it is manipulated by a client

public class OnGrabbedBehaviour : NetworkBehaviour
{

    NetworkIdentity netID;

    public bool grabbed;

    [SerializeField]
    public LocalPlayerController localPlayerController;

    private Transform left;
    private Transform right;

    // Use this for initialization
    void Start()
    {
        netID = gameObject.GetComponent<NetworkIdentity>();
    }

    // Update is called once per frame
    void Update()
    {

        // GO´s behaviour when it is in a grabbed state (owned by a client) should be defined here
        if (grabbed && netID.hasAuthority)
            gameObject.transform.position = right.position;
    }

    // called first time when the GO gets grabbed by a player
    public void OnGrabbed()
    {
        //Debug.Log("on grabbed in onGrabbedBehaviour");
        grabbed = true;

        left = localPlayerController.actor.GetComponent<Actor>().character.left;
        right = localPlayerController.actor.GetComponent<Actor>().character.right;

        SetKinematic(true);
    }

    // called when the GO gets released by a player
    public void OnReleased()
    {
        grabbed = false;

        SetKinematic(false);
    }

    public void SetKinematic(bool b)
    {
        //RpcDebug("server sets kinematic: " + b + " and gravity: " + !b);
        gameObject.GetComponent<Rigidbody>().isKinematic = b;
        gameObject.GetComponent<Rigidbody>().useGravity = !b;
    }

    [ClientRpc]
    public void RpcDebug(string log)
    {
        Debug.Log(log);
    }
}
