﻿using UnityEngine;
using System.Collections;

// This script defines conditions that are necessary for the Vive player to grab a shared object
// TODO: values of these four boolean variables can be changed either directly here or through other components
// AuthorityManager of a shared object should be notifyed from this script

public class ViveGrab : MonoBehaviour
{

    AuthorityManager am; // to communicate the fulfillment of grabbing conditions

    [SerializeField]
    public LocalPlayerController localPlayerController;

    // conditions for the object control here
    //bool leftHandTouching = false;
    bool rightHandTouching = false;
    //bool leftTriggerDown = false;
    bool rightTriggerDown = false;
    bool grabbed = false;

    private SteamVR_TrackedController controllerLeft;
    private SteamVR_TrackedController controllerRight;

    // Use this for initialization
    void Start()
    {
        controllerLeft = localPlayerController.left.gameObject.GetComponent<SteamVR_TrackedController>();
        controllerRight = localPlayerController.right.gameObject.GetComponent<SteamVR_TrackedController>();

        //localPlayerController.actor.GetComponent<Actor>().character.right.gameObject.AddComponent<TouchDetector>();
        //localPlayerController.actor.GetComponent<Actor>().character.left.gameObject.AddComponent<TouchDetector>();
    }

    // Update is called once per frame
    void Update()
    {

        //if (gameObject.Equals(localPlayerController.actor.GetComponent<Actor>().character.left.gameObject.GetComponent<TouchDetector>().touched))
        //{
        //    // Debug.Log("left hand touching");
        //    leftHandTouching = true;
        //}
        //else
        //{
        //    leftHandTouching = false;
        //}

        if (gameObject.Equals(localPlayerController.actor.GetComponent<Actor>().character.right.gameObject.GetComponent<TouchDetector>().touched))
        {
            // Debug.Log("right hand touching");
            rightHandTouching = true;
        }
        else
        {
            rightHandTouching = false;
        }

        //if (controllerLeft.triggerPressed)
        //{
        //    // Debug.Log("press left");
        //    leftTriggerDown = true;
        //}
        //else
        //{
        //    leftTriggerDown = false;
        //}

        if (controllerRight.triggerPressed)
        {
            // Debug.Log("press right");
            rightTriggerDown = true;
        }
        else
        {
            rightTriggerDown = false;
        }

        // Debug.Log("leftHandTouching: " + leftHandTouching + ", rightHandTouching: " + rightHandTouching + ", leftTriggerDown: " + leftTriggerDown + ", rightTriggerDown: " + rightTriggerDown);

        if (rightHandTouching && rightTriggerDown) // 
        {
            // notify AuthorityManager that grab conditions are fulfilled
            if (!grabbed)
            {
                if (am == null)
                    am = gameObject.GetComponent<AuthorityManager>();
                am.grabbedByPlayer = true;
                grabbed = true;
                OnGrabbedBehaviour ogb = gameObject.GetComponent<OnGrabbedBehaviour>() as OnGrabbedBehaviour;
                ogb.localPlayerController = this.localPlayerController;
                ogb.OnGrabbed();

            }
        }
        else
        {
            // grab conditions are not fulfilled
            if (grabbed)
            {
                if (am == null)
                    am = gameObject.GetComponent<AuthorityManager>();
                am.releasedByPlayer = true;
                grabbed = false;
                gameObject.GetComponent<OnGrabbedBehaviour>().OnReleased();
            }
        }
    }
}
