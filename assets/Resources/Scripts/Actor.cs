﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
using Leap.Unity;
public class Actor : NetworkBehaviour
{
    public GameObject goPrefab;
    public Character character;
    public new Transform transform;
    Spawner spawner;



    [SyncVar]
    private string prefabName = "Actor";

    //this part is for object sharing
    //*******************************
    List<NetworkIdentity> sharedObjects; // shared objects on the server or localActor
    //*******************************


    protected virtual void Awake()
    {
        transform = base.transform;
    }

    // Use this for initialization
    void Start()
    {
        sharedObjects = new List<NetworkIdentity>();

        if (isServer || isLocalPlayer)
        {
            if (isLocalPlayer)
            {
                // Inform the local player about his new character
               
                LocalPlayerController.Singleton.SetActor(this);
                CmdInitialize(prefabName);
            }


            //this part is for object sharing
            //*******************************
            if (isServer)
            {
                // find objects that can be manipulated 
                // TIPP : you can use a specific tag for all GO's that can be manipulated by players

                foreach (GameObject go in GameObject.FindGameObjectsWithTag("shared"))
                {
                    //Debug.Log("Server: assign gameObject to network list");
                    sharedObjects.Add(go.GetComponent<NetworkIdentity>());
                }
            }
            if (isLocalPlayer)
            {
                // find objects that can be manipulated 
                // assign this Actor to the localActor field of the AuthorityManager component of each shared object

                spawner = GameObject.Find("Spawner").GetComponent<Spawner>();
                spawner.actor = this;

                foreach (GameObject go in GameObject.FindGameObjectsWithTag("shared"))
                {
                    //Debug.Log("Client: assign gameObject to network list");
                    go.GetComponent<AuthorityManager>().AssignActor(this);
                    sharedObjects.Add(go.GetComponent<NetworkIdentity>());

                    spawner.AssignProperties(go);
                }
            }
            //*******************************

        }
        else
        {
            // Initialize on startup
            Initialize(prefabName);
        }

    }

    public void Update()
    {

    }

    /// <summary>
    /// Updates the actor position and rotation.
    /// This function should be called only by the <see cref="LocalPlayerController"/>.
    /// </summary>
    public void UpdateActorLeft(Vector3 leftPos, Quaternion leftRot) //runs only on LocalPlayer!
    {
        if (character != null)
        {
            character.UpdateCharacterLeft(leftPos, leftRot);
        }
    }

    /// <summary>
    /// Updates the actor position and rotation.
    /// This function should be called only by the <see cref="PlayerController"/>.
    /// </summary>
    public void UpdateActorRight(Vector3 rightPos, Quaternion rightRot) //runs only on LocalPlayer!
    {
        if (character != null)
        {
            character.UpdateCharacterRight(rightPos, rightRot);
        }
    }

    public void SetRightCharacterActive(bool active)
    {
        character.SetRightActive(active);
    }

    public void SetLeftCharacterActive(bool active)
    {
        character.SetLeftActive(active);
    }

    /// <summary>
    /// Initialize the player locally.
    /// </summary>
    /// <param name="prefab">Prefab character name.</param>
    public void Initialize(string prefab)
    {
        prefabName = prefab;
        name = name.Replace("(Clone)", "");

    }

    /// <summary>
    /// Spawns the character of actor on all clients.
    /// This runs on server only.
    /// </summary>
    /// <param name="prefab">Prefab name of the character.</param>
    private void SpawnCharacter(string prefab)
    {
        // Spawn character
        GameObject modelPrefab = Resources.Load("Prefabs/" + prefab) as GameObject;
        GameObject model = (GameObject)Instantiate(modelPrefab, transform.position, transform.rotation) as GameObject;
        NetworkServer.Spawn(model);

        // Attach character to player
        AttachCharacter(model.GetComponent<Character>());
    }

    /// <summary>
    /// Initializes the character on server to inform all clients. 
    /// This command calls the Initialize() method and spawns the character.
    /// </summary>
    [Command]
    public void CmdInitialize(string prefab)
    {
        if (prefab.Length > 0)
        {
            CreateCharacter(prefab);
        }
    }

    /// <summary>
    /// Creates the character and initializes on server.
    /// </summary>
    /// <param name="prefab">The character prefab name.</param>
    [Server]
    public void CreateCharacter(string prefab)
    {
        SpawnCharacter(prefab);
        Initialize(prefab);
    }

    /// <summary>
    /// Main routine to attach the character to this actor
    /// This runs only on Server.
    /// </summary>
    /// <param name="newCharacter">New character to attach.</param>
    [Server]
    public void AttachCharacter(Character newCharacter)
    {
        newCharacter.AttachToActor(netId);
    }


    //this part is for object sharing
    // fill in the implementation
    //*******************************

    // should only be run on localPlayer (by the AuthorityManager of a shared object)
    // ask the server for the authority over an object with NetworkIdentity netID
    public void RequestObjectAuthority(NetworkIdentity netID)
    {
        //Debug.Log("request object authority");
        if (isLocalPlayer)
            CmdAssignObjectAuthorityToClient(netID);
    }

    // should only be run on localPlayer (by the AuthorityManager of a shared object)
    // ask the server to remove the authority over an object with NetworkIdentity netID
    public void ReturnObjectAuthority(NetworkIdentity netID)
    {
        if (isLocalPlayer)
            CmdRemoveObjectAuthorityFromClient(netID);
    }

    // run on the server
    // netID is NetworkIdentity of a shared object the authority if which should be passed to the client
    [Command]
    void CmdAssignObjectAuthorityToClient(NetworkIdentity netID)
    {
        RpcDebug("Assign Request on server");
        if (isServer)
        {
            netID.gameObject.GetComponent<OnGrabbedBehaviour>().grabbed = true;
            netID.gameObject.GetComponent<OnGrabbedBehaviour>().SetKinematic(true);
            netID.gameObject.GetComponent<AuthorityManager>().AssignClientAuthority(connectionToClient);
        }
    }

    // run on the server
    // netID is NetworkIdentity of a shared object the authority if which should be removed from the client
    [Command]
    void CmdRemoveObjectAuthorityFromClient(NetworkIdentity netID)
    {
        RpcDebug("Remove Request on server");
        if (isServer)
        {
            netID.gameObject.GetComponent<AuthorityManager>().RemoveClientAuthority(connectionToClient);
            netID.gameObject.GetComponent<OnGrabbedBehaviour>().grabbed = false;
            netID.gameObject.GetComponent<OnGrabbedBehaviour>().SetKinematic(false);
        }
    }
    //*******************************


    [Command]
    public void CmdSpawn(Vector3 spawnPosition)
    {
        //spawnPosition = new Vector3(Random.Range(-0.5f, 0.5f), 1.0f, Random.Range(-.5f, 0.5f));
        //ViveGrab vg = box.AddComponent<ViveGrab>() as ViveGrab;
        //vg.playerController = this.playerController;
        //

        // GameObject go = Instantiate(testPrefab);


        Quaternion spawnRotation = Quaternion.Euler(0, 0, 0);
        GameObject go = Instantiate(goPrefab, spawnPosition, spawnRotation);
        //go.tag = "NEW";
        NetworkServer.Spawn(go);
        
        sharedObjects.Add(go.GetComponent<NetworkIdentity>());
        RpcCompleteItem(go);
        // return go;
        //NetworkServer.SpawnWithClientAuthority(go, this.gameObject);
    }

    [ClientRpc]
    public void RpcCompleteItem(GameObject go)
    {
        spawner = GameObject.Find("Spawner").GetComponent<Spawner>();
        spawner.AssignProperties(go);
    }

    [ClientRpc]
    public void RpcDebug(string log)
    {
        Debug.Log(log);
    }
}
